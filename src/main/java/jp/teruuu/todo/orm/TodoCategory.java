package jp.teruuu.todo.orm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Data;

import org.hibernate.validator.constraints.Length;

@Data
@Entity
@Table(name="todo_category")
@SequenceGenerator(name="todo_category_id_seq", sequenceName="todo_category_id_seq")
public class TodoCategory implements Serializable{
	private static final long serialVersionUID = -6790132668286958668L;

	@Id
    @SequenceGenerator(sequenceName = "todo_category_id_seq", name = "todo_category_id_seq", allocationSize=1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="todo_category_id_seq")
	@Column(name="id")
	private Long id;
	
	@Length(max=512)
	@Column(name="name")
	private String name;
	
	@Column(name="index")
	private int index;
	
	
	public TodoCategory(){}
	public TodoCategory(String name, int index){
		this.name = name;
		this.index = index;
	}
}
