package jp.teruuu.todo.orm;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Data;

import org.hibernate.validator.constraints.Length;

@Data
@Entity
@Table(name="todo")
@SequenceGenerator(name="todo_id_seq", sequenceName="todo_id_seq")
public class Todo implements Serializable{
	private static final long serialVersionUID = -6790132668286958668L;

	@Id
    @SequenceGenerator(sequenceName = "todo_id_seq", name = "todo_id_seq", allocationSize=1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="todo_id_seq")
	@Column(name="id")
	private Long id;
	
	@Column(name="category_id")
	private Long categoryId;
	
	@Length(max=512)
	@Column(name="title")
	private String title;
	
	@Length(max=512)
	@Column(name="text")
	private String text;
	
	@Column(name="index")
	private int index;
	
	
	public Todo(){};
	public Todo(Long categoryId, String title, String text, int index){
		this.categoryId = categoryId;
		this.title = title;
		this.text = text;
		this.index = index;
	}
}
