package jp.teruuu.todo.repository;

import java.util.List;

import jp.teruuu.todo.orm.TodoCategory;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface TodoCategoryRepo extends JpaRepository<TodoCategory, Long> {
	
	@Query("select t from TodoCategory t order by t.index")
    public List<TodoCategory> findAll();
	
	@Query("select t from TodoCategory t where t.id = :id")
    public TodoCategory findById(@Param("id") Long id);

}
