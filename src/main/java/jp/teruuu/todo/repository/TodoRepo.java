package jp.teruuu.todo.repository;

import java.util.List;

import jp.teruuu.todo.orm.Todo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface TodoRepo extends JpaRepository<Todo, Long> {

	
	@Query("select t from Todo t where t.id = :id")
    public Todo findById(@Param("id") long id);
	
	@Query("select t from Todo t where t.categoryId = :category_id order by t.index")
    public List<Todo> findByCategoryId(@Param("category_id") long categoryId);
}
