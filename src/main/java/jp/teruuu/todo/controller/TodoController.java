package jp.teruuu.todo.controller;

import java.util.ArrayList;
import java.util.List;

import jp.teruuu.todo.orm.Todo;
import jp.teruuu.todo.orm.TodoCategory;
import jp.teruuu.todo.repository.TodoCategoryRepo;
import jp.teruuu.todo.repository.TodoRepo;
import lombok.Data;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

//cross origin requestはサーバサイドで許可する必要がある
//@CrossOrigin(origins = "http://localhost:8080") 
@CrossOrigin
@RestController
@RequestMapping("/todo")
public class TodoController {
	
	@Autowired
	TodoCategoryRepo todoCategoryRepo;
	
	@Autowired
	TodoRepo todoRepo;
	
	@RequestMapping(value={"", "/"})
	public Object index() {
		return this.getAllTodo();
	}
	
	
	@RequestMapping(value="/delete", method={RequestMethod.POST, RequestMethod.GET})
	public Object deleteTodo(@RequestParam("todoId") String todoId) {
		todoRepo.delete(Long.valueOf(todoId));
		return this.index();
	}
	
	@RequestMapping(value="/add", method={RequestMethod.POST, RequestMethod.GET})
	public Object addTodoId(@RequestParam("categoryId") String categoryId,
			@RequestParam("title") String title,
			@RequestParam("text") String text) {
		
		Long categoryIdNum = Long.valueOf(categoryId);
		Todo insertTodo = new Todo(categoryIdNum, title, text, this.todoRepo.findByCategoryId(categoryIdNum).size() + 1);
		todoRepo.save(insertTodo);
		return this.index();
	}
	
	@RequestMapping(value="/add_list", method={RequestMethod.POST, RequestMethod.GET})
	public Object addList(@RequestParam("listTitle") String listTitle){
		TodoCategory todoCategory = new TodoCategory(listTitle, this.todoCategoryRepo.findAll().size() + 1);
		this.todoCategoryRepo.save(todoCategory);
		return this.index();
	}
	
	@RequestMapping(value="/move", method={RequestMethod.POST, RequestMethod.GET})
	public Object moveTodo(@RequestParam("categoryId") String categoryId,
			@RequestParam("todoId") String todoId) {
		Todo todo = todoRepo.findById(Long.valueOf(todoId));
		todo.setCategoryId(Long.valueOf(categoryId));
		todoRepo.save(todo);
		return null;
	}
	
	@RequestMapping(value="/move_list", method={RequestMethod.POST, RequestMethod.GET})
	public Object moveList(@RequestParam("categoryId") String categoryId,
			@RequestParam("index") String index) {
		TodoCategory target = this.todoCategoryRepo.findById(Long.valueOf(categoryId));
		List<TodoCategory> allCategory = this.todoCategoryRepo.findAll();
		
		int targetNextIndex = Integer.valueOf(index);
		int targetLastIndex = target.getIndex();
		int big = targetNextIndex >= targetLastIndex ? targetNextIndex : targetLastIndex;
		int small = targetNextIndex >= targetLastIndex ? targetLastIndex : targetNextIndex;
		boolean smallMove = targetNextIndex < targetLastIndex;
		for(TodoCategory current: allCategory){
			if(current == target){
				continue;
			}
			if(big >= current.getIndex() && current.getIndex() >= small){
				if(smallMove){
					current.setIndex(current.getIndex() + 1);
				}else{
					current.setIndex(current.getIndex() - 1);
				}
				this.todoCategoryRepo.save(current);
			}
		}
		target.setIndex(targetNextIndex);
		this.todoCategoryRepo.save(target);
		return null;
	}
	
	@RequestMapping(value="/delete_list", method={RequestMethod.POST, RequestMethod.GET})
	public Object deleteList(@RequestParam("categoryId") String categoryId){
		Long categoryIdNum = Long.valueOf(categoryId);
		TodoCategory category = this.todoCategoryRepo.findById(categoryIdNum);
		List<Todo> todoList = this.todoRepo.findByCategoryId(categoryIdNum);
		for(Todo todo:todoList){
			this.todoRepo.delete(todo);
		}
		this.todoCategoryRepo.delete(category);
		return this.index();
	}
	
	@RequestMapping(value="/test", method={RequestMethod.POST, RequestMethod.GET})
	public List<TodoDataSet> test() {
		return this.testData();
	}
	
	
	private List<TodoDataSet> testData(){
		List<TodoDataSet> ret = new ArrayList<TodoDataSet>();

		Todo card1 = new Todo(Long.valueOf("1"), "use kanban", "使う", 1);
		Todo card2 = new Todo(Long.valueOf("1"), "use kanban", "使う", 1);
		Todo card3 = new Todo(Long.valueOf("1"), "use kanban", "使う", 1);
		Todo card4 = new Todo(Long.valueOf("1"), "use kanban", "使う", 1);
		
		TodoDataSet todo1 = new TodoDataSet(Long.valueOf("1"), "todo", 1);
		TodoDataSet todo2 = new TodoDataSet(Long.valueOf("2"), "doing", 2);
		TodoDataSet todo3 = new TodoDataSet(Long.valueOf("3"), "done", 3);
		
		todo1.addTodo(card1);
		todo1.addTodo(card2);
		todo2.addTodo(card3);
		todo3.addTodo(card4);

		ret.add(todo1);
		ret.add(todo2);
		ret.add(todo3);
		return ret;
	}
	
	
	private List<TodoDataSet> getAllTodo(){
		List<TodoDataSet> ret = new ArrayList<TodoDataSet>();
		List<TodoCategory> allCategory = this.todoCategoryRepo.findAll();
		for(TodoCategory category:allCategory){
			TodoDataSet todoDataSet = new TodoDataSet(category);
			List<Todo> todoList = this.todoRepo.findByCategoryId(category.getId());
			todoDataSet.setCards(todoList);
			ret.add(todoDataSet);
		}
		return ret;
	}
	
	@Data
	private class TodoDataSet{
		Long id;
		String name;
		int index;
		List<Todo> cards = new ArrayList<Todo>();
		
		public TodoDataSet(TodoCategory todo){
			this.id = todo.getId();
			this.name = todo.getName();
			this.index = todo.getIndex();
		}
		public TodoDataSet(Long categoryId, String title, int index){
			this.id = categoryId;
			this.name = title;
			this.index = index;
		}
		public void addTodo(Todo todo){
			this.cards.add(todo);
		}
	}

	
	
}
